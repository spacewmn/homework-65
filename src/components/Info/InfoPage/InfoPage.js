import React from 'react';

const InfoPage = props => {

    return (
        <div className="InfoPage">
            <h1>{props.title}</h1>
            <p>{props.content}</p>
        </div>
    );
};

export default InfoPage;