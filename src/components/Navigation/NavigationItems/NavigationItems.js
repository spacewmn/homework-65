import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";
import {Nav, Navbar} from 'reactstrap';

const NavigationItems = () => (
    <Navbar color="light" light expand="md">
        <Nav className="mr-auto" navbar>
            <NavigationItem to={"/admin"}>Admin</NavigationItem>
            <NavigationItem to="/pages/home">Home</NavigationItem>
            <NavigationItem to="/pages/about">About</NavigationItem>
            <NavigationItem to="/pages/production">Production</NavigationItem>
            <NavigationItem to="/pages/contacts">Contacts</NavigationItem>
            <NavigationItem to="/pages/support">Support</NavigationItem>
        </Nav>
    </Navbar>
);

export default NavigationItems;