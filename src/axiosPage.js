import axios from 'axios';

const axiosPage = axios.create({
    baseURL: 'https://burger-project-js7.firebaseio.com/'
});

export default axiosPage;