import React, {useEffect, useMemo, useState} from 'react';
import { Spinner } from 'reactstrap';

const WithErrorHandler = (WrappedComponent, axios) => {

    return function WithErrorHandler(props) {
        const [loading, setLoading] = useState(false);

        const icReq = useMemo(() => {
            axios.interceptors.request.use(request => {
                setLoading(true);
                console.log('yehgf');

                return request;
            });
        }, []);

        const icRes = useMemo(() => {
            axios.interceptors.response.use(response => {
                setLoading(false);
                console.log('ye');
                return response;
            });
        }, []);

        useEffect(() => {
            return () => {
                axios.interceptors.request.eject(icReq)
                axios.interceptors.response.eject(icRes)

            };
        }, [icReq, icRes]);

        return (
            <>
                {loading ? <Spinner color="primary" /> : null}
                <WrappedComponent {...props} />
            </>

        )
    };
};

export default WithErrorHandler;